package com.spring.codeblog.controllers;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.codeblog.models.Post;
import com.spring.codeblog.services.IPostService;

@Controller
public class PostsController {

	@Autowired
	IPostService _postService;
	
	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	public ModelAndView getPosts() {
		ModelAndView mv = new ModelAndView("posts/index");
		
		List<Post> posts = _postService.findAll();
		
		mv.addObject("posts", posts);
		
		return mv;
	}
	
	@RequestMapping(value = "/posts/{id}", method = RequestMethod.GET)
	public ModelAndView getPostDetails(@PathVariable("id") long id) {
		ModelAndView mv = new ModelAndView("posts/detail");
		
		Post post = _postService.findById(id);
		
		mv.addObject("post", post);
		
		return mv;
	}
	
	@RequestMapping(value = "/posts/new", method = RequestMethod.GET)
	public String getPostForm() {

		return "posts/create";
	}
	
	@RequestMapping(value = "/posts/new", method = RequestMethod.POST)
	public String savePost(@Valid Post post, BindingResult result, RedirectAttributes attributes) {
		
		if (result.hasErrors()) {
			attributes.addFlashAttribute("mensagem", "Verique o(s) campo(s) obrigatorio(s) !");
			return "redirect:/posts/new";
		}
		
		post.setData(LocalDate.now());
		_postService.save(post);
		return "redirect:/posts";
	}
	
}
