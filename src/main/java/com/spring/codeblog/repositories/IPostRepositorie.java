package com.spring.codeblog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.codeblog.models.Post;

public interface IPostRepositorie extends JpaRepository<Post, Long>{
	
}
