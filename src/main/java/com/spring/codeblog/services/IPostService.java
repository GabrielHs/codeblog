package com.spring.codeblog.services;

import java.util.List;

import com.spring.codeblog.models.Post;

public interface IPostService {

	List<Post> findAll();
	
	Post findById(long id);
	
	Post save(Post post);
}
