package com.spring.codeblog.services.imple;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.codeblog.models.Post;
import com.spring.codeblog.repositories.IPostRepositorie;
import com.spring.codeblog.services.IPostService;

@Service
public class PostServiceImplement implements IPostService {

	@Autowired
	IPostRepositorie _postRepository;
	
	@Override
	public List<Post> findAll() {
		
		return _postRepository.findAll();
	}

	@Override
	public Post findById(long id) {
		
		return _postRepository.findById(id).get();
	}

	@Override
	public Post save(Post post) {
		
		return _postRepository.save(post);
	}

}
